require 'gem2deb/rake/spectask'

Gem2Deb::Rake::RSpecTask.new(:spec) do |spec|
  spec.pattern = FileList['./spec/**/*_spec.rb']
end

task :default => :spec
